#!/usr/bin/env python3

import argparse

import logging

from xml.etree import ElementTree

import json

import requests

from fuzzywuzzy import process

from fuzzywuzzy import fuzz

import bibtexparser
from bibtexparser.bparser import BibTexParser
from bibtexparser.customization import convert_to_unicode

import sys

from os import path


def read_bibtex(string):
    """Read list of journal titles from bibtex file

    Args
    ------------------------------------------------------
    string: str, bibtex file data

    Returns
    -----------------------------------------------------
    journals: set of strings,  All journal titles in file

    """

    parser = BibTexParser()
    parser.customization = convert_to_unicode
    bib_database = bibtexparser.loads(string, parser=parser)

    entries_journal = [entry for entry in bib_database.entries if
                       'journal' in entry]

    return set([entry['journal'] for entry in entries_journal])


def get_nlm_xml(journal):
    """Query NLM Catalog for journal and return the xml data.

    Args
    ------------------------------------------------------
    journal: str, journal title

    Returns
    -----------------------------------------------------
    xml: str, The somewhat raw document returned by NLM, which is not valid
        XML. See parse function for details.

    """

    # Read the POST params from disk and set the query terms
    dataname = path.join(path.split(__file__)[0], 'params.json')
    with open(dataname) as datafile:
        postdata = json.load(datafile)

    postdata["EntrezSystem2.PEntrez.DbConnector.Term"] = journal
    postdata["term"] = journal

    # Run the query
    req = requests.post("http://www.ncbi.nlm.nih.gov/nlmcatalog/",
                        data=postdata)

    # Raise an exception if we got a 4xx or 5xx error
    req.raise_for_status()

    return req.text


def parse_nlm(xml):
    """Extract NLM abreviations from NLM catalog search result.

    Args
    ------------------------------------------------------
    xml: str, The xml source

    Returns
    -----------------------------------------------------
    Either:
        * abbrev: str, The journal name abbreviation.
        * ids: list of str, NLM IDs of type 'serial' if multiple search results
        * None if no search result

    Raises:
    -----------------------------------------------------
    Probably an IndexError if unexpected xml is fed in.

    """

    # The NLM response is not actually a raw, valid XML document, but rather
    # XLM with multiple root nodes, wrapped in a <pre> tag.
    # Therefore, we do a first parsing pass by extracting and decoding the
    # contents of the pre-tag, and wrapping it in a root tag.

    valid_xml = '\n'.join(('<root>',
                           ElementTree.fromstring(xml).text,
                           '</root>'
                           ))

    root = ElementTree.fromstring(valid_xml)

    entries = root.findall('NCBICatalogRecord/NLMCatalogRecord')
    abbrevs = {e.find('./TitleMain/Title'): e.find('./MedlineTA')
               for e in entries}

    # Filter out entries for which there is no title or abbreviation
    abbrevs = {title.text: abbrev.text for title, abbrev in abbrevs.items()
               if title is not None and abbrev is not None
               }

    abbrevs = {cleantitle(t): a for t, a in abbrevs.items()}

    return abbrevs


def cleantitle(title):
    # If there is a colon, keep only what is before
    title = title.split(':')[0].strip()

    # Remove trailing period
    if title[-1] == '.':
        title = title[:-1]

    return title


def wscore(s1, s2):
    """Add a tiebreaker to fuzz.WRatio"""
    score = fuzz.WRatio(s1, s2)
    tbscore = fuzz.ratio(s1, s2)
    return score*10 + tbscore


def write_out(rdict, where):
    for journal, abbrev in rdict.items():
        where.write(" = ".join((journal, abbrev)) + '\n')


def get_nlm_abbrev(journal):
    try:
        xml = get_nlm_xml(journal)
    except requests.HTTPError as e:
        logging.error("HTTP error for journal \"%s\": %s"
                      % (journal, e.args[0])
                      )
        return None

    try:
        parse_result = parse_nlm(xml)
    except:
        logging.error("Parsing error for journal \"%s\""
                      % (journal,)
                      )
        return None

    if not parse_result:
        logging.error("No result for journal \"%s\""
                      % (journal,)
                      )
        return None

    # If we got here, parse_result is a dictionary of journal titles
    # and their corresponding abbreviations.
    # We use fuzzy string matching to get the best match.

    best = process.extractOne(journal, parse_result.keys(), scorer=wscore)
    best_abbrev = parse_result[best[0]]

    return best_abbrev


def main():
    parser = argparse.ArgumentParser(
        description='Query NLM Catalog for journal abbreviations')

    parser.add_argument(
        'bibtex_file',
        type=argparse.FileType('rt'),
        help='Bibtex file to source',
        )

    args = parser.parse_args()

    journals = read_bibtex(args.bibtex_file.read())
    args.bibtex_file.close()

    abbrevs = {j: get_nlm_abbrev(j) for j in journals}

    # Don't write out results for which no results were found
    abbrevs = {j: a for j, a in abbrevs.items() if a}

    write_out(abbrevs, sys.stdout)

if __name__ == "__main__":
    main()
