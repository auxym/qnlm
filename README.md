# qnlm.py

A short python (3.3) script to query NLM for journal abbreviations, allowing
fuzzy-matching of journal names.

**NOTE** I wrote this script for my master's dissertation in 2015, and
unfortunately have no idea if it still works. The NLM might very well have
changed their API.

## Dependencies

* [fuzzywuzzy](https://github.com/seatgeek/fuzzywuzzy)
* [requests](http://docs.python-requests.org/en/master/)
* [bibtexparser](https://github.com/sciunto-org/python-bibtexparser)

Optional:

* [python-Levenshtein](https://github.com/miohtama/python-Levenshtein)

All can be installed from pip.

## Usage

```
$ python qnlm.py -h


usage: qnlm.py [-h] bibtex_file

Query NLM Catalog for journal abbreviations

positional arguments:
  bibtex_file  Bibtex file to source

optional arguments:
  -h, --help   show this help message and exit
```
